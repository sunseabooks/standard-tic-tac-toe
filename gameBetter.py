board = [1, 2, 3, 4, 5, 6, 7, 8, 9]

moveX = []
moveO = []

winRow = [[1,4,7], [2,5,8], [3,6,9]]
winCol = [[1,2,3], [4,5,6], [7,8,9]]
winDag = [[1,5,9],[3,5,7], [11,12,13]]
checkMaster = [winRow, winCol, winDag]

def print_board(entries):
    line = "+---+---+---+"
    output = line
    n = 0
    for entry in entries:
        if n % 3 == 0:
            output = output + "\n| "
        else:
            output = output + " | "
        output = output + str(entry)
        if n % 3 == 2:
            output = output + " |\n"
            output = output + line
        n = n + 1
    print(output)
    print()


def moveTotal(moveX, moveO):
    return len(moveX) + len(moveO)

def currentCheck(moveTotal):
    if moveTotal == 0:
        return "X"
    elif moveTotal%2 == 0:
        return "X"
    else:
        return "O"

def moveCheck(current_player):
    if current_player == "X":
        return moveX
    else:
        return moveO

def winStart():
    i = 0
    movelist = moveCheck(current_player)
    def winCheck(checkmovelist):
        checkScore = [0,0,0,0]
        while checkScore[0] < 3:
            id = checkScore[0]
            for items in checkMaster[i][id]:
                if items in checkmovelist:
                    checkScore[id+1] = checkScore[id+1] +1
                if checkScore[1] ==3 or checkScore[2] ==3 or checkScore[3] == 3:
                    return "WIN"
            checkScore[0] = checkScore[0] +1

    while i < 3:
        if winCheck(movelist) == "WIN":
            print(current_player, "has won")
            exit()
        i = i+1

def spaceCheck(response):
    space_number = int(response) - 1
    if type(response) == type(board[space_number]):
        return space_number
    else:
        responseNew = int(input("Current space is taken, Player " + current_player + " please pick another? "))
        return spaceCheck(responseNew)


for move_number in range(1, 10):
    current_player = currentCheck(moveTotal(moveX, moveO))
    print_board(board)

    response = int(input("Where would " + current_player + " like to move? "))
    space_number = spaceCheck(response)
    print(current_player)
    board[space_number] = current_player

    if moveTotal(moveX, moveO) == 0:
        moveX.append(response)
    elif current_player == "X":
        moveX.append(response)
    else:
        moveO.append(response)

    if moveTotal(moveX, moveO) > 4:
        winStart()
    if moveTotal(moveX, moveO) == 9:
        print("It's a tie!")
        exit()
